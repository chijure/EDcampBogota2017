# EDcampBogota2017

Se debe crear el archivo `docker-compose.yml` usando como guía `docker-compose.yml.example`, indicando la ruta (volume) del anfitrión (lado izquierdo) en la que se almacenarán los datos de postgresql.

Para construir/descargar y arrancar los contenedores de la aplicación:
```bash
docker-compose up -d
```

Para detener y destruir los contenedores de la aplicación:
```bash
docker-compose down
```

Para ejecutar comandos dentro de los contenedores de la aplicación se usan los nombres de los servicios:
- nginx
- php
- postgresql

```bash
docker-compose exec SERVICIO COMANDO
```

Ejemplos:

  - Para postgresql:
  ```bash
  docker-compose exec postgresql psql --version
  docker-compose exec postgresql psql -U postgres
  ```
  
  - Para mostrar la lista de opciones de artisan:
  ```bash
  docker-compose exec php php artisan list
  ```
  
  - Para ver los logs de los contenedores de la aplicación:
  ```bash
  docker-compose logs -f
  ```

>Si no existe la base de datos, se debe crear el usuario y la base de datos en postgresql.
```bash
docker-compose exec postgresql psql -U postgres
postgres=# CREATE USER edcamp PASSWORD 'edcamp';
CREATE ROLE
postgres=# CREATE DATABASE edcamp OWNER edcamp;
CREATE DATABASE
postgres=# \q
```

Para restaurar la base de datos se puede hacer lo siguiente:
```bash
docker-compose exec postgresql psql -U edcamp
edcamp=> \i /database/sql/2017-05-04-edcamp.sql
edcamp=> \i /database/sql/2017-06-12-roles.sql
edcamp=> \q
```

## Pasos para poner en marcha Laravel
```bash
# Instalar la dependencias de composer
docker-compose exec php composer install

# Si no existe el archivo .env, se debe crear y
# configurar en él la conexión a la base de datos
docker-compose exec php cp .env.example .env

# Generar la llave de la aplicación
docker-compose exec php php artisan key:generate

# Configurar los permisos de las carpetas:
docker-compose exec php chmod -R 777 storage/
docker-compose exec php chmod -R 777 bootstrap/cache/

# Crear enlace simbólico de storage con las rutas absolutas del contenedor
docker-compose exec php ln -s /var/www/html/storage/app/public /var/www/html/public/storage
```

La aplicación es accesible desde:
- `http://127.0.0.1:2017`
- `http://localhost:2017`
