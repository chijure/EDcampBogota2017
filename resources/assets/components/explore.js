import VanillaModal from 'vanilla-modal'

export function explore() {
  const d = document,
    w = window,
    mq = w.matchMedia('(min-width: 64em)')

  function isMobile (mq) {
    if (!mq.matches) {
      new VanillaModal()
    }
  }

  mq.addListener(isMobile)
  isMobile(mq)

  d.querySelector('a[href="#modal-tags"]').addEventListener( 'click', e => e.preventDefault() )
}
